using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class CorProdutoController : ControllerBase
    {
        private readonly SiCContext _context;

        public CorProdutoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/CorProduto
        [HttpGet]
        public IEnumerable<CorProduto> GetCorProduto()
        {
            return _context.CoresProduto;
        }

        // GET: api/CorProduto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCorProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var corProduto = await _context.CoresProduto.FindAsync(id);

            if (corProduto == null)
            {
                return NotFound();
            }

            return Ok(corProduto);
        }

        // PUT: api/CorProduto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCorProduto([FromRoute] int id, [FromBody] CorProduto corProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != corProduto.ID)
            {
                return BadRequest();
            }

            _context.Entry(corProduto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CorProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CorProduto
        [HttpPost]
        public async Task<IActionResult> PostCorProduto([FromBody] CorProduto corProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CoresProduto.Add(corProduto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCorProduto", new { id = corProduto.ID }, corProduto);
        }

        // DELETE: api/CorProduto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCorProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var corProduto = await _context.CoresProduto.FindAsync(id);
            if (corProduto == null)
            {
                return NotFound();
            }

            _context.CoresProduto.Remove(corProduto);
            await _context.SaveChangesAsync();

            return Ok(corProduto);
        }

        private bool CorProdutoExists(int id)
        {
            return _context.CoresProduto.Any(e => e.ID == id);
        }
    }
}