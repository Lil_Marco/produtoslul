using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Repositories;
using SiCProject.Models;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AcabamentoController : ControllerBase
    {
        private readonly SiCContext _context;
        private AcabamentoRepository _repository;

        public AcabamentoController(SiCContext context)
        {
            _context = context;
            _repository = new AcabamentoRepository(_context);
        }

        // GET: api/Acabamento
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAcabamentos()
        {
            List<Acabamento> lista = _repository.GetAll();
            List<AcabamentoDTO> dtos = new List<AcabamentoDTO>();

            foreach (Acabamento a in lista)
            {
                dtos.Add(_repository.getInfoAcabamentoDTO(a));
            }
            return Ok(dtos);
        }

        // GET: api/Acabamento/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Acabamento a = _repository.GetByID(id);

            if (a == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoAcabamentoDTO(a));
        }

        // PUT: api/Acabamento/5
        [HttpPut("{id}")]
		[AllowAnonymous]
        public async Task<IActionResult> PutAcabamento([FromRoute] int id, [FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != acabamento.ID)
            {
                return BadRequest();
            }
            if (_repository.Put(acabamento))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Acabamento
        [HttpPost]
		[AllowAnonymous]
        public async Task<IActionResult> PostAcabamento([FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Acabamento acab = _repository.Post(acabamento);

            if (acab != null)
            {
                return Ok(_repository.getInfoAcabamentoDTO(acab));
            }
            return BadRequest("Nome existente! Insira outro");
        }

        // DELETE: api/Acabamento/5
        [HttpDelete("{id}")]
		[AllowAnonymous]
        public async Task<IActionResult> DeleteAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.Delete(id))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        private bool AcabamentoExists(int id)
        {
            return _context.Acabamentos.Any(e => e.ID == id);
        }


        [HttpGet("nome={nome}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAcabamentoByName([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<Acabamento> lista = _repository.GetByNome(nome);

            if (lista == null)
            {
                return NotFound();
            }

            List<AcabamentoDTO> dtos = new List<AcabamentoDTO>();
            foreach (Acabamento a in lista)
            {
                dtos.Add(_repository.getInfoAcabamentoDTO(a));
            }
            return Ok(dtos);

        }

        [HttpGet("{id}/Precos")]
        [AllowAnonymous]
        public async Task<IActionResult> GetPrecosAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<PrecoAcabamento> lista = _repository.GetPrecosAcabamento(id);

            return Ok(lista);
        }
    }
}