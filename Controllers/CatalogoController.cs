﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;
using SiCProject.Repositories;

namespace SiCProject
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogoController : ControllerBase
    {
        private readonly SiCContext _context;
        private CatalogoRepository repo;

        public CatalogoController(SiCContext context)
        {
            _context = context;
            repo = new CatalogoRepository(context);
        }

        // GET: api/Catalogo
        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<CatalogoDTO> GetCatalogo()
        {

            List<CatalogoDTO> lDTO = new List<CatalogoDTO>();
            foreach (Catalogo c in repo.GetAll())
            {

                lDTO.Add(repo.getInfoCotalogoDTO(c));


            }


            return lDTO;
        }

        // GET: api/Catalogo/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetCatalogo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Catalogo catalogo = repo.GetByID(id);

            if (catalogo == null)
            {
                return NotFound();
            }

            return Ok(repo.getInfoCotalogoDTO(catalogo));
        }

        // PUT: api/Catalogo/5
        [HttpPut("{id}")]
		[AllowAnonymous]
        public async Task<IActionResult> PutCatalogo([FromRoute] int id, [FromBody] Catalogo catalogo)
        {
                if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != catalogo.ID)
            {
                return BadRequest();
            }

            if(repo.Put(catalogo)){
                 return NoContent();
            }else{
                return NotFound();
            }
        }
        

        // POST: api/Catalogo
        [HttpPost]
		[AllowAnonymous]
        public async Task<IActionResult> PostCatalogo([FromBody] Catalogo catalogo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Catalogo cat = repo.Post(catalogo);

            return Ok(repo.getInfoCotalogoDTO(cat));
        }

        // DELETE: api/Catalogo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalogo([FromRoute] int id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(repo.Delete(id)){
                return NoContent();
            }else{
                return NotFound();
            }
        }

        private bool CatalogoExists(int id)
        {
            return _context.Catalogo.Any(e => e.ID == id);
        }
    }
}