﻿using System;

/* Versão atual do EF nãu suporta ENUMS
 *
 *SOURCE: https://stackoverflow.com/questions/32542356/how-to-save-enum-in-database-as-string
 */


public static class UserRole
{
    public const string CLIENTE = "CLIENTE";
    public const string GESTOR_CATALOGOS = "GESTOR_CATALOGOS";
    public const string SYS_ADMIN = "SYS_ADMIN";
}
