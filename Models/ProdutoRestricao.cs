using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class ProdutoRestricao
    {
        public int ID {get; set;}

        public int ProdutoID {get; set;}

        public int RestricaoID {get; set;}
    }
}