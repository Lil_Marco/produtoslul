using System;

namespace SiCProject.Models.Restricoes
{
    public abstract class Restricao
    {
        public int ID {get; set;}

        public abstract bool checkRestricao(String info);
    }
}