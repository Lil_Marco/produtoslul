using System;

public class MateriaisDiferentesException : Exception
{
    public MateriaisDiferentesException() : base() { }
    public MateriaisDiferentesException(string message) : base(message) { }
    public MateriaisDiferentesException(string message, System.Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client. 
    protected MateriaisDiferentesException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
}