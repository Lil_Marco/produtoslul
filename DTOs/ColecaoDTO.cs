﻿using SiCProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.DTOs
{
    public class ColecaoDTO
    {
        public int ID { get; set; }
        public string linhaEstetica { get; set; }
        public List<ProdutoDTO> lProdutos { get; set; }


        public ColecaoDTO(int iD, string linhaEstetica, List<ProdutoDTO> lProdutos){
            this.ID = iD;
            this.linhaEstetica = linhaEstetica;
            this.lProdutos = lProdutos;
        }
    }
}
