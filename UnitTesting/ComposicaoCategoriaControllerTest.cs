using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ComposicaoCategoriaControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            ComposicaoCategoria c1 = new ComposicaoCategoria();

            ComposicaoCategoria c2 = new ComposicaoCategoria();

            _context.ComposicoesCategorias.Add(c1);
            _context.ComposicoesCategorias.Add(c2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new ComposicaoCategoriaController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<ComposicaoCategoria> lol = _context.ComposicoesCategorias;
                using(IEnumerator<ComposicaoCategoria> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutComposicaoCategoria()
        {
            using (var _context = InitDBSet("Test_PutComposicaoCategoria_M"))
            {
                var _mController = new ComposicaoCategoriaController(_context);
                Assert.Equal(0, await _context.ComposicoesCategorias.CountAsync());

                ComposicaoCategoria m = new ComposicaoCategoria();

                await _context.ComposicoesCategorias.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.ComposicoesCategorias.CountAsync());

                int mid = 1;

                var acab = await _mController.PutComposicaoCategoria(mid, m);

            }
        }
        [Fact]
        public async void Test_PostComposicaoCategoria()
        {
            using (var _context = InitDBSet("Test_PostComposicaoCategoria"))
            {
                DB(_context);
                var _mController = new ComposicaoCategoriaController(_context);

                ComposicaoCategoria m = new ComposicaoCategoria();

                var c = await _mController.PostComposicaoCategoria(m);
                Assert.IsType<CreatedAtActionResult>(c);

            }
        }

        [Fact]
        public async void Test_DeleteComposicaoCategoria()
        {
            using (var _context = InitDBSet("Test_DeleteComposicaoCategoria_M"))
            {
                ComposicaoCategoria m = new ComposicaoCategoria();

                _context.ComposicoesCategorias.Add(m);

                _context.SaveChanges();

                var _mController = new ComposicaoCategoriaController(_context);

                var c = await _mController.DeleteComposicaoCategoria(m.ID);
                Assert.IsType<OkObjectResult>(c);
            }
        }

        [Fact]
        public async void Test_GetComposicaoCategoria()
        {
            using (var _context = InitDBSet("Test_GetComposicaoCategoria"))
            {
                DB(_context);
                var _mController = new ComposicaoCategoriaController(_context);

                ComposicaoCategoria m = await _context.ComposicoesCategorias.FirstAsync();

                var c = await _mController.GetComposicaoCategoria(m.ID);
                Assert.IsType<OkObjectResult>(c);
            }
        }
    }
}