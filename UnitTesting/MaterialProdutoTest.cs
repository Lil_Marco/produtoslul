using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class MaterialProdutoTest
    {
        private SiCContext InitDBSet(String DbName)
        {
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context)
        {

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor()
        {
            using (var _context = InitDBSet("Test_construtor"))
            {

                DB(_context);
                MaterialProduto mp = new MaterialProduto();

                int ProdutoId = 2;
                int MaterialID = 1;

                mp.ProdutoID = 2;
                mp.MaterialID = 1;

                Assert.Equal(ProdutoId, mp.ProdutoID);
                Assert.Equal(MaterialID, mp.MaterialID);
            }
        }
    }
}