using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class DimensaoTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor(){
            using(var _context = InitDBSet("Test_construtor")){

                DB(_context);

                int alturaId = 1;
                int larguraID = 2;
                int profundidadeID = 3;

                Dimensao comp = new Dimensao();
                comp.alturaID = 1;
                comp.larguraID = 2;
                comp.profundidadeID = 3;

                Assert.Equal(alturaId, comp.alturaID);
                Assert.Equal(larguraID, comp.larguraID);
                Assert.Equal(profundidadeID, comp.profundidadeID);
            }
        }
    }
}