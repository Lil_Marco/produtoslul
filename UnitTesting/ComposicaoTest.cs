using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ComposicaoTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor(){
            using(var _context = InitDBSet("Test_construtor")){

                DB(_context);

                int produtoPaiID = 1;
                int produtoFilhoID = 2;
                bool obrigatoria = true;

                Composicao comp = new Composicao();
                comp.ProdutoPaiID = 1;
                comp.ProdutoFilhoID = 2;
                comp.Obrigatoria = false;

                Assert.Equal(produtoPaiID, comp.ProdutoPaiID);
                Assert.Equal(produtoFilhoID, comp.ProdutoFilhoID);
                Assert.NotEqual(obrigatoria, comp.Obrigatoria);
            }
        }
    }
}