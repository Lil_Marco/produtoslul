using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ColecaoTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor(){
            using(var _context = InitDBSet("Test_construtor")){

                DB(_context);

                int linhaId = 1;
                LinhaEstetica linhaEstetica = new LinhaEstetica();
                List<Produto> lProdutos = new List<Produto>();


                Colecao colecao = new Colecao();
                colecao.linhaEsteticaID = 1;
                colecao.linhaEstetica = linhaEstetica;
                colecao.lProdutos = new List<Produto>();

                Assert.Equal(linhaId, colecao.linhaEsteticaID);
                Assert.Equal(lProdutos, colecao.lProdutos);
                Assert.Equal(linhaEstetica, colecao.linhaEstetica);
            }
        }
    }
}