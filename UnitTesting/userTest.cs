using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class RestricaoTest
    {
        private SiCContext InitDBSet(String DbName)
        {
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context)
        {

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor()
        {
            using (var _context = InitDBSet("Test_construtor"))
            {

                DB(_context);
                User user = new User();

                String Username = "Besugos";
                String Password = "LAPR5";
                String role = "bla";
                String token = "[0-9]";
                String email = "besugos@isep.ipp.pt";

                user.Username = "Besugos";
                user.Password = "LAPR5";
                user.role = "bla";
                user.Token = "[0-9]";
                user.email = "besugos@isep.ipp.pt";

                Assert.Equal(Username, user.Username);
                Assert.Equal(Password, user.Password);
                Assert.Equal(role, user.role);
                Assert.Equal(token, user.Token);
                Assert.Equal(email, user.email);
            }
        }
    }
}