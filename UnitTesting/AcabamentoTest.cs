using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class AcabamentoTest
    {
        private SiCContext InitDBSet(String DbName)
        {
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context)
        {

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor()
        {
            using (var _context = InitDBSet("Test_construtor"))
            {

                DB(_context);
                Acabamento acabamento = new Acabamento();
                Material m = new Material();

                String nomeAcab = "verniz";
                float Incremento = 12.0f;
                int materialID = m.ID;



                acabamento.Nome = "verniz";
                acabamento.Incremento = 12.0f;
                acabamento.MaterialId = m.ID;

                Assert.Equal(nomeAcab, acabamento.Nome);
                Assert.Equal(Incremento, acabamento.Incremento);
                Assert.Equal(materialID, acabamento.MaterialId);
            }
        }
    }
}