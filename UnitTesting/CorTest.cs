using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class CorTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor(){
            using(var _context = InitDBSet("Test_construtor")){

                DB(_context);

                int rgbRed = 1;
                int rgbVerde = 2;
                int rgbAzul = 3;
                string nome = "BOM DIA";

                Cor comp = new Cor();
                comp.rgbRed = 1;
                comp.rgbVerde = 2;
                comp.rgbAzul = 3;
                comp.Nome = "BOM DIA 2";

                Assert.Equal(rgbRed, comp.rgbRed);
                Assert.Equal(rgbVerde, comp.rgbVerde);
                Assert.Equal(rgbAzul, comp.rgbAzul);
                Assert.NotEqual(nome, comp.Nome);
            }
        }
    }
}