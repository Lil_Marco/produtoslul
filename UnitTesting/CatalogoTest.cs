using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class CatalogoTest
    {
        private SiCContext InitDBSet(String DbName)
        {
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context)
        {

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor()
        {
            using (var _context = InitDBSet("Test_construtor"))
            {

                DB(_context);
                Catalogo catalogo = new Catalogo();

                String nome = "Catalogo Produto";
                Produto prod = new Produto();
                prod.Nome = "Armario";

                catalogo.nome = "Catalogo Produto";
                catalogo.lProdutos = new List<Produto>();
                catalogo.lProdutos.Add(prod);

                Assert.Equal(nome, catalogo.nome);
            }
        }
    }
}