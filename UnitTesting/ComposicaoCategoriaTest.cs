using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ComposicaoCategoriaTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor(){
            using(var _context = InitDBSet("Test_construtor")){

                DB(_context);

                int categoriaPaiID = 1;
                int categoriaFilhoID = 2;

                ComposicaoCategoria compCat = new ComposicaoCategoria();
                compCat.CategoriaPaiID = 1;
                compCat.CategoriaFilhoID = 2;

                Assert.Equal(categoriaPaiID, compCat.CategoriaPaiID);
                Assert.Equal(categoriaFilhoID, compCat.CategoriaFilhoID);
            }
        }
    }
}