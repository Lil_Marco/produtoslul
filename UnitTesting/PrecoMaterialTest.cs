using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class PrecoMaterialTest
    {
        private SiCContext InitDBSet(String DbName)
        {
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context)
        {

            _context.SaveChanges();
        }

        [Fact]
        public void Test_construtor()
        {
            using (var _context = InitDBSet("Test_construtor"))
            {

                DB(_context);
                PrecoMaterial pm = new  PrecoMaterial();

                float preco = 20.5f;
                int matId = 1;

                pm.preco = 20.5f;
                pm.MaterialId = 1;

                Assert.Equal(preco, pm.preco);
                Assert.Equal(matId, pm.MaterialId);
            }
        }
    }
}