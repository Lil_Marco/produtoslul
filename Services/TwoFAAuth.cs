﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Services
{
    interface TwoFAAuth
    {

          void Authenticate(User user, string token);
    }
}
