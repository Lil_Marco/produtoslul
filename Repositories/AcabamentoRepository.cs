using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;


namespace SiCProject.Repositories
{
    public class AcabamentoRepository : AcabamentoRepositoryInterface
    {

        private readonly SiCContext _context;
        private PrecoAcabamentoRepository pr;

        public AcabamentoRepository(SiCContext context)
        {
            _context = context;
            pr = new PrecoAcabamentoRepository(_context);
        }

        public AcabamentoDTO getInfoAcabamentoDTO(Acabamento a)
        {
            return new AcabamentoDTO(a.ID, a.Nome, a.Incremento, a.MaterialId);
        }

        public List<PrecoAcabamento> GetPrecosAcabamento(int id)
        {
            List<PrecoAcabamento> lista = new List<PrecoAcabamento>();
            IQueryable<PrecoAcabamento> lista1 = _context.PrecoAcabamentos.Where(pM => pM.AcabamentoId.Equals(id));
            foreach (PrecoAcabamento pm in lista1)
            {
                lista.Add(pm);
            }
            return lista;

        }

        public List<Acabamento> GetAll()
        {
            List<Acabamento> acab = new List<Acabamento>();
            this.atualizarPreco();

            foreach (Acabamento acabamento in _context.Acabamentos)
            {
                acab.Add(acabamento);
            }
            return acab;
        }

        public Acabamento GetByID(int id)
        {
            this.atualizarPreco();
            Acabamento acabamento = _context.Acabamentos.Find(id);

            return acabamento;
        }

        public List<Acabamento> GetByNome(string nome)
        {
            this.atualizarPreco();
            IQueryable<Acabamento> acabamento = _context.Acabamentos.Where(a => a.Nome.Equals(nome));
            List<Acabamento> lista = acabamento.ToList();

            return lista;
        }

        public Acabamento Post(Acabamento obj)
        {
            List<string> lista = this.GetAllNames();

            bool flag = false;
            foreach (string m in lista)
            {
                if (m == obj.Nome)
                {
                    flag = true;
                }
            }
            if (flag == false)
            {
                _context.Acabamentos.Add(obj);
                _context.SaveChangesAsync();

                return obj;
            }
            return null;
        }

        public bool Put(Acabamento obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var acabamento = _context.Acabamentos.Find(id);

            if (acabamento == null)
            {
                return false;
            }

            _context.Acabamentos.Remove(acabamento);
            _context.SaveChangesAsync();

            return true;
        }

        private bool AcabamentoExists(int id)
        {
            return _context.Acabamentos.Any(e => e.ID == id);
        }

        public List<string> GetAllNames()
        {
            List<string> nomes = new List<string>();
            foreach (Acabamento a in _context.Acabamentos)
            {
                nomes.Add(a.Nome);
            }
            return nomes;
        }
        public void atualizarPreco()
        {
            DateTime date = new DateTime();
            date = DateTime.Now;

            List<PrecoAcabamento> pMs = new List<PrecoAcabamento>();
            pMs = pr.GetAll();
            List<Acabamento> mats = new List<Acabamento>();

            int count = 0;
            float preco = 0;
            DateTime dataPreco = new DateTime();

            foreach (Acabamento m in _context.Acabamentos)
            {
                preco = 0;
                if (pMs.Any())
                {
                    foreach (PrecoAcabamento pM in pMs)
                    {
                        if (pM.AcabamentoId == m.ID)
                        {
                            if (DateTime.Compare(date, pM.data) > 0)
                            {
                                if (count == 0)
                                {
                                    preco = pM.preco;
                                    dataPreco = pM.data;
                                    count++;
                                }
                                else
                                {
                                    if (DateTime.Compare(pM.data, dataPreco) > 0)
                                    {
                                        preco = pM.preco;
                                        dataPreco = pM.data;
                                    }
                                }

                            }
                        }
                    }
                    m.Incremento = preco;
                    _context.Entry(m).State = EntityState.Modified;
                }
            }
            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                Console.WriteLine("Erro");
            }
        }


    }
}