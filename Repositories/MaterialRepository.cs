using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;
using SiCProject.Repositories;


namespace SiCProject.Repositories
{
    public class MaterialRepository : MaterialRepositoryInterface
    {

        private readonly SiCContext _context;

        private PrecoMaterialRepository pr;

        public MaterialRepository(SiCContext context)
        {
            _context = context;
            pr = new PrecoMaterialRepository(_context);
        }

        public MaterialDTO getInfoMaterialDTO(Material m)
        {

            List<String> acabs = new List<String>();
            foreach (Acabamento acabamento in _context.Acabamentos)
            {
                if (acabamento.MaterialId == m.ID)
                {
                    acabs.Add(_context.Acabamentos.Find(acabamento.ID).Nome);
                }
            }
            return new MaterialDTO(m.ID, m.Nome, acabs, m.Preco);
        }

        public List<Material> GetAll()
        {
            List<Material> mats = new List<Material>();
            this.atualizarPreco();

            foreach (Material material in _context.Materiais)
            {
                mats.Add(material);
            }
            return mats;
        }

        public Material GetByID(int id)
        {

            this.atualizarPreco();
            Material material = _context.Materiais.Find(id);

            return material;
        }

        public List<Material> GetByNome(string nome)
        {
            this.atualizarPreco();
            IQueryable<Material> material = _context.Materiais.Where(m => m.Nome.Equals(nome));//.FirstOrDefault<Produto>();
            List<Material> lista = material.ToList();

            return lista;
        }

        public Material Post(Material obj)
        {
            List<string> lista = this.GetAllNames();

            bool flag = false;
            foreach (string nome in lista)
            {
                if (nome == obj.Nome)
                {
                    flag = true;
                }
            }
            if (flag == false)
            {
                _context.Materiais.Add(obj);
                _context.SaveChangesAsync();

                return obj;
            }
            return null;
        }

        public bool Put(Material obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var material = _context.Materiais.Find(id);

            if (material == null)
            {
                return false;
            }

            _context.Materiais.Remove(material);
            _context.SaveChangesAsync();

            return true;
        }

        private bool MaterialExists(int id)
        {
            return _context.Materiais.Any(e => e.ID == id);
        }

        public List<string> GetAllNames()
        {
            List<string> nomes = new List<string>();
            foreach (Material m in _context.Materiais)
            {
                nomes.Add(m.Nome);
            }
            return nomes;
        }

        public List<PrecoMaterial> GetPrecosMaterial(int id)
        {
            List<PrecoMaterial> lista = new List<PrecoMaterial>();
            IQueryable<PrecoMaterial> lista1 = _context.PrecoMateriais.Where(pM => pM.MaterialId.Equals(id));
            foreach (PrecoMaterial pm in lista1)
            {
                lista.Add(pm);
            }
            return lista;

        }

        public List<Acabamento> GetAcabamentosFromMaterial(int id)
        {
            IQueryable<Acabamento> acabamento = _context.Acabamentos.Where(a => a.MaterialId.Equals(id));//.FirstOrDefault<Produto>();
            List<Acabamento> lista = acabamento.ToList();

            return lista;
        }
        public void atualizarPreco()
        {
            DateTime date = new DateTime();
            date = DateTime.Now;

            List<PrecoMaterial> pMs = new List<PrecoMaterial>();
            pMs = pr.GetAll();
            List<Material> mats = new List<Material>();

            int count = 0;
            float preco = 0;
            DateTime dataPreco = new DateTime();

            foreach (Material m in _context.Materiais)
            {
                preco = 0;
                if (pMs.Any())
                {
                    foreach (PrecoMaterial pM in pMs)
                    {
                        if (pM.MaterialId == m.ID)
                        {
                            if (DateTime.Compare(date, pM.data) > 0)
                            {
                                if (count == 0)
                                {
                                    preco = pM.preco;
                                    dataPreco = pM.data;
                                    count++;
                                }
                                else
                                {
                                    if (DateTime.Compare(pM.data, dataPreco) > 0)
                                    {
                                        preco = pM.preco;
                                        dataPreco = pM.data;
                                    }
                                }
                            }
                        }
                    }
                    m.Preco = preco;
                    _context.Entry(m).State = EntityState.Modified;

                }
            }
            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                Console.WriteLine("Erro");
            }
        }
    }


}