using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;
using SiCProject.Repositories;


namespace SiCProject.Repositories
{
    public class PrecoMaterialRepository : PrecoMaterialRepositoryInterface
    {

        private readonly SiCContext _context;
        private MaterialRepository mr;

        public PrecoMaterialRepository(SiCContext context)
        {
            _context = context;
        }

        public PrecoMaterialDTO getInfoPrecoMaterialDTO(PrecoMaterial c)
        {
            return new PrecoMaterialDTO(c.ID, c.preco, c.data, c.MaterialId);
        }

        public List<PrecoMaterial> GetAll()
        {
            List<PrecoMaterial> cor = new List<PrecoMaterial>();
            foreach (PrecoMaterial corr in _context.PrecoMateriais)
            {
                cor.Add(corr);
            }
            return cor;
        }

        public PrecoMaterial GetByID(int id)
        {
            PrecoMaterial cor = _context.PrecoMateriais.Find(id);

            return cor;
        }

        public PrecoMaterial Post(PrecoMaterial obj)
        {
            DateTime date = new DateTime();
            date = DateTime.Now;
            if (obj.data == DateTime.MinValue)
            {
                //unassigned
                obj.data = date;
            }

            _context.PrecoMateriais.Add(obj);


            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return null;
            }

            return obj;
        }

        public bool Put(PrecoMaterial obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var cor = _context.PrecoMateriais.Find(id);

            if (cor == null)
            {
                return false;
            }

            _context.PrecoMateriais.Remove(cor);
            _context.SaveChangesAsync();

            return true;
        }

        private bool PrecoMaterialExists(int id)
        {
            return _context.PrecoMateriais.Any(e => e.ID == id);
        }
    }
}